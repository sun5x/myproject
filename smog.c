//程序头函数
#include <reg52.h>
//显示函数
#include <display.h>

//宏定义
#define uint unsigned int 
#define uchar unsigned char
#define Data_ADC0809 P1
 
//管脚声明
sbit LED_R= P2^2;
sbit LED_Y= P2^1;
sbit LED_G= P2^0;
sbit Feng = P2^5;
sbit san=P3^4;//控制风扇
//ADC0809
sbit ST=P3^3;
sbit EOC=P3^6;
sbit OE=P3^2;
//按键
sbit Key1=P2^6;
sbit Key2=P2^7;
sbit Key3=P3^7;


//函数声明
extern uchar ADC0809();
extern void Key();

void delay(uint z)
{
	uint i,j;
	for(i=0;i<z;i++)
	for(j=0;j<121;j++);
}


//酒精含量变量
uchar temp=0;

//指示灯变量
//5到10绿灯亮起
uchar GL=2;
//10到20黄灯亮起
uchar YL=15;
//20到255红灯亮起
uchar RL=30;
//蜂鸣器变量
uchar FF=60;
//按钮模式|0 正常|1 G|2 Y|3 R|4 F|
uchar Mode=0;

void main()
{
	Init1602();
	
	while(1)
	{
		temp=ADC0809();

		if(Mode==0)
		{		
			Display_1602(GL,YL,RL,FF,temp);
			//指示灯显示和蜂鸣器控制判断
			if(temp>=GL&&temp<YL)
			{
				LED_G=0;
			}
			else
			{
				LED_G=1;
			}
			if(temp>=YL&&temp<RL)
			{
				LED_Y=0;
			}
			else
			{
				LED_Y=1;
			}
			if(temp>=RL&&temp<=255)
			{
				LED_R=0;
			}
			else
			{
				LED_R=1;
			}	
			if(temp>=FF&&temp<=255)
			{
				Feng=0;
				san=0;
			}
			else
			{
				Feng=1;
				san=1;
			}
		}
		Key();
	}
}
//ADC0809读取信息
uchar ADC0809()
{
	uchar temp_=0x00;
	//初始化高阻太

	OE=0;
	//转化初始化
	ST=0;
	//开始转换
	ST=1;
	ST=0;
	//外部中断等待AD转换结束
	while(EOC==0)
	//读取转换的AD值
	OE=1;
	temp_=Data_ADC0809;
	OE=0;
	return temp_;
}

void Key()
{
	//模式选择
	if(Key1==0)
	{
		while(Key1==0);
		Feng=0;
		if(Mode==4)
		{
			Mode=0;
			Feng=1;
		}
		else
		{
   		write_com(0x38);//屏幕初始化
   		write_com(0x0d);//打开显示 无光标 光标闪烁
   		write_com(0x06);//当读或写一个字符是指针后一一位
			Mode++;
			switch(Mode)
			{
				case 1:
				{
					write_com(0x80+4);//位置
					Feng=1;
					break;
				}
				case 2:
				{
					write_com(0x80+9);//位置
					Feng=1;
					break;
				}
				case 3:
				{
					write_com(0x80+14);//位置
					Feng=1;
					break;
				}
				case 4:
				{
					write_com(0x80+0x40+4);//位置
					Feng=1;
					break;
				}
			}
		}
	}
	if(Key2==0&&Mode!=0)
	{
		while(Key2==0);
		//|0 正常|1 G|2 Y|3 R|4 F|
		Feng=0;
		switch(Mode)
		{
			case 1:
			{
				if(GL<YL-1)
				{
					GL++;
					write_com(0x80+2);
					write_data('0'+GL/100);
					write_data('0'+GL/10%10);
					write_data('0'+GL%10);
					write_com(0x80+4);//位置
				}
				Feng=1;
				break;
			}
			case 2:
			{
				if(YL<RL-1)
				{
					YL++;
					write_com(0x80+7);
					write_data('0'+YL/100);
					write_data('0'+YL/10%10);
					write_data('0'+YL%10);
					write_com(0x80+9);//位置
				}
				Feng=1;
				break;				
			}
			case 3:
			{
				if(RL<255-1)
				{
					RL++;
					write_com(0x80+12);
					write_data('0'+RL/100);
					write_data('0'+RL/10%10);
					write_data('0'+RL%10);
					write_com(0x80+14);//位置
				}
				Feng=1;
				break;				
			}
			case 4:
			{
				if(FF<255-1)
				{
					FF++;
					write_com(0x80+0x42);
					write_data('0'+FF/100);
					write_data('0'+FF/10%10);
					write_data('0'+FF%10);
					write_com(0x80+0x40+4);//位置
				}
				Feng=1;
				break;				
			}		
		}
	}
	if(Key3==0&&Mode!=0)
	{
		while(Key3==0);
		Feng=0;
		//|0 正常|1 G|2 Y|3 R|4 F|
		switch(Mode)
		{
			case 1:
			{
				if(GL>0)
				{
					GL--;
					write_com(0x80+2);
					write_data('0'+GL/100);
					write_data('0'+GL/10%10);
					write_data('0'+GL%10);
					write_com(0x80+4);//位置
				}
				Feng=1;
				break;
			}
			case 2:
			{
				if(YL>GL+1)
				{
					YL--;
					write_com(0x80+7);
					write_data('0'+YL/100);
					write_data('0'+YL/10%10);
					write_data('0'+YL%10);
					write_com(0x80+9);//位置
				}
				Feng=1;
				break;				
			}
			case 3:
			{
				if(RL>YL+1)
				{
					RL--;
					write_com(0x80+12);
					write_data('0'+RL/100);
					write_data('0'+RL/10%10);
					write_data('0'+RL%10);
					write_com(0x80+14);//位置
				}
				Feng=1;
				break;				
			}
			case 4:
			{
				if(FF>0)
				{
					FF--;
					write_com(0x80+0x42);
					write_data('0'+FF/100);
					write_data('0'+FF/10%10);
					write_data('0'+FF%10);
					write_com(0x80+0x40+4);//位置
				}
				Feng=1;
				break;				
			}		
		}
	}
	delay(200);
	write_com(0x38);//屏幕初始化
	write_com(0x0c);//打开显示 无光标 无光标闪烁
}

